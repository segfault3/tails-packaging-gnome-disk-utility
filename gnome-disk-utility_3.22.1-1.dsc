-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gnome-disk-utility
Binary: gnome-disk-utility
Architecture: any
Version: 3.22.1-1
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders: Michael Biebl <biebl@debian.org>, Martin Pitt <mpitt@debian.org>, Sjoerd Simons <sjoerd@debian.org>,
Homepage: http://git.gnome.org/cgit/gnome-disk-utility/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-utopia/gnome-disk-utility.git
Vcs-Git: git://anonscm.debian.org/pkg-utopia/gnome-disk-utility.git
Build-Depends: appstream-util, debhelper (>= 10), docbook-xsl, gnome-settings-daemon-dev (>= 3.8), intltool, libcanberra-gtk3-dev (>= 0.1), libdvdread-dev (>= 4.2.0), libglib2.0-dev (>= 2.31.0), libgtk-3-dev (>= 3.16.0), liblzma-dev (>= 5.0.5), libnotify-dev (>= 0.7), libpwquality-dev (>= 1.0.0), libsecret-1-dev (>= 0.7), libsystemd-dev (>= 209), libudisks2-dev (>= 2.1.1), pkg-config, xsltproc
Package-List:
 gnome-disk-utility deb admin optional arch=any
Checksums-Sha1:
 ad9264be6990729b4d2ae3d5ba6378fa121a2dc1 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 503b49c01513ef9e109b1f4be9f2c0ed30bfa035 6148 gnome-disk-utility_3.22.1-1.debian.tar.xz
Checksums-Sha256:
 a3a4a187549f812e3837ae17dd9fa60afcacd84389d696058de254fe18b51ec3 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 dec04ad4d9cb371f1cb00266d0d94620f48d2974e54574d2130928f47f3ea011 6148 gnome-disk-utility_3.22.1-1.debian.tar.xz
Files:
 ecb9ace36964c279f8093f5e34cf3799 1455508 gnome-disk-utility_3.22.1.orig.tar.xz
 9a14219e338af644c2481a798514d55a 6148 gnome-disk-utility_3.22.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCAAGBQJYIe+PAAoJEGrh3w1gjyLclcMQAJzHZ5ZFmOKLYkofRcRj9d5Z
vUtjQK13Z+AH5kt/qpVESJB6yLsbHdz854B5mSK/7o610m/hQf9DX0UWB8zJbJ9m
3xlM/Gw+Roql07FBQXs6GtIg64xlf40Me7wPDtC0IUhK14Jr5VsEWsoDWzGQIXow
7YESPTSLkrQ9Rz5+tw8VyEnRVSJ6ycgA5gNi4S09OH5zeYZE6FzdjXfX3plkUZi8
tfS4A9taJLvpHktD3lk3nqPqb91tdalkXtwVE/PI/xtjReYuKPIK6MdwMSAc+Exz
fRKkl4bUEROdiJyhW/ASNc8OxlXHBcXL1aWLX0m3ehtuKV08uTHdKftdY7SPRqIF
VMTHjUzQBItX/B+2kykmAUSTwWK7Kzcym3naEWfOgjtoKN8y4Ce6hG7QhleZIKNL
F7N20rfo/XbCnWpdNgE7WLF/jDoKJSEbZETqz/WjP/iEhcH5oWM9Y4rstGaDw69i
+RTImlj3UYp1hPnn3xDpojmyAPnoxfoHKLFtX9Mwk6E5nTNVuphh7lVF3XQkQ2Tx
sdVyW7rXN2Zvrd+LdeUc1jpwKUANfB3LUpg7Hko9NflJKEKtMUbwAg0MKlcvFTfS
1lXAQTX2ePOsagtsCJRFxvptdzoARxA7EuTA993RgmVbKvVLfjF2Ft67Lalm0iCD
jCGg+xBRTjE3qzQrrF6x
=zU74
-----END PGP SIGNATURE-----
